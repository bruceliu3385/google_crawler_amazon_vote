# coding=utf-8
from locale import *
import csv
import time
from selenium import webdriver
from openpyxl import load_workbook


def is_element_by_id(self, element_id):
    flag = True
    try:
        self.find_element_by_id(element_id)
        return flag
    except:
        flag = False
        return flag


def set_driver():
    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument("--test-type")
    options.add_experimental_option('excludeSwitches', ['enable-automation'])

    prefs = {"profile.managed_default_content_settings.images": 2}
    options.add_experimental_option("prefs", prefs)
    path = './chromedriver.exe'
    driver = webdriver.Chrome(executable_path=path,options=options)
    return driver


workbook = load_workbook(u'掉评筛选.xlsx')
booksheet = workbook.active
rows = booksheet.rows
columns = booksheet.columns


line_continue = input("上次检测截止行数：")

driver = set_driver()

i = 0
# 迭代所有的行
for row in rows:
    i = i + 1
    line = [col.value for col in row]
    cp_id = booksheet.cell(row=i, column=1).value
    record_id = booksheet.cell(row=i, column=2).value
    user = booksheet.cell(row=i, column=3).value
    product_name = booksheet.cell(row=i, column=4).value
    order_no = booksheet.cell(row=i, column=5).value
    asin = booksheet.cell(row=i, column=9).value
    # 获取第i行评论链接
    review_url = (booksheet.cell(row=i, column=6).value ).strip()

    # 验证review_url 是否是404 ,判断是否掉评
    if i == 1:
        continue

    if i < int(line_continue):
        continue

    try:
        if review_url.startswith('http') and 'review' in review_url and 'amazon' in review_url:
            driver.get(review_url)
            driver.implicitly_wait(5)
            time.sleep(2)

            isDiaoPing = 0
            description = '待检测'
            for link in driver.find_elements_by_xpath('//a'):
                url = link.get_attribute('href')
                if url is None:
                    continue
                if '404_logo' in url:
                    isDiaoPing = 1

            if isDiaoPing == 0:
                description = '未掉评'
                line.append("未掉评")

                # 判断asin是否正确
                if is_element_by_id(driver, 'cr-state-object'):
                    asin_str = driver.find_element_by_id('cr-state-object').get_attribute('data-state')
                    if asin not in asin_str:
                        isDiaoPing = 4
                        description = '链接与产品不符'
            else:
                description = '已掉评'
                line.append("已掉评")

            # 判断是否出现验证码
            if is_element_by_id(driver, 'captchacharacters'):
                isDiaoPing = 2
                description = '待检测'


            # 写入文件
            if order_no is None:
                order_no = ''
            else:
                order_no = order_no.strip()
            line_text = [(cp_id, record_id, user, product_name, order_no, review_url, description, isDiaoPing)]
        else:
            line_text = [(cp_id, record_id, user, product_name, order_no, review_url, '评论乱填', 3)]
    except:
        line_text = [(cp_id, record_id, user, product_name, order_no, review_url, '待检测', 2)]

    # 制作表格
    with open("掉评检测结果.csv", "a+", newline='', encoding='utf-8') as file:
        csv_file = csv.writer(file)
        csv_file.writerows(line_text)
    file.close()
    print(line_text)


# 制作数据
driver.quit()
