#coding=utf-8
import time
from locale import *
import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
import csv
import random

#具体发送消息方法
def send_message(username, title, message):
    #到发送站内消息页面
    driver.get("https://forums.huaren.us/usercppostpm.aspx")
    WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'msgto')))
    driver.find_element_by_id("msgto").clear()
    driver.find_element_by_id("msgto").send_keys(username)
    WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'subject')))
    driver.find_element_by_id("subject").clear()
    driver.find_element_by_id("subject").send_keys(title)
    WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'message')))
    driver.find_element_by_id("message").clear()
    driver.find_element_by_id("message").send_keys(message)
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.ID, 'sendmsg')))
    time.sleep(5)
    driver.find_element_by_id('sendmsg').click()



setlocale(LC_NUMERIC, 'English_US')

loginUrl = "https://forums.huaren.us/showforum.aspx?forumid=398"

username = input("Input userName:")
print("userName:", username)
password = input("Input passWord:")
print("passWord:", password)


#要导入的包含所有用户名的文件名
username_file_input = input("Input username file:")
print("username file is:"+username_file_input)

#要导入的组织好的消息内容
message_info_input = input("Input message file:")
print("message file is:"+ message_info_input)

#将所有用户存入一个list中
username_list = []
with open(username_file_input,'r',encoding='utf8',newline='') as csvfile:
    reader=csv.reader(csvfile)
    for line in reader:
        if line not in username_list:
            username_list.append(line[0])
print(username_list)

#将所有消息导入到message_list
message_list = []
with open(message_info_input,'r', newline='') as csvfile:
    reader=csv.reader(csvfile)
    for line in reader:
        if line not in message_list:
            message_list.append(line)
print(message_list)

#firefoxProfie = FirefoxProfile()
#firefoxProfie.set_preference('permissions.default.image', 2)
#driver = webdriver.Firefox(firefoxProfie)
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
prefs = {"profile.managed_default_content_settings.images": 2}
options.add_experimental_option("prefs", prefs)
driver = webdriver.Chrome(chrome_options=options)
driver.get(loginUrl)

#用户名
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'ls_username')))
driver.find_element_by_id("ls_username").clear()
driver.find_element_by_id("ls_username").send_keys(username)
#密码
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'ls_password')))
driver.find_element_by_id('ls_password').clear()
driver.find_element_by_id('ls_password').send_keys(password)
WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.ID, 'dlbtn')))
time.sleep(1)
driver.find_element_by_id('dlbtn').click()



#循环去重后的username_list,调用发送消息
for uname in username_list:
    messageInfo = random.choice(message_list)
    title = messageInfo[0]
    message = messageInfo[1]
    send_message(uname,title,message)
    time.sleep(15)
