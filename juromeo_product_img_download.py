# coding:utf-8
import MySQLdb
import requests
import time
import os
import threading
from bs4 import BeautifulSoup



# 下载图片
def img_download(img_name):
    net_img_path = "https://img-shefavors.oss-us-west-1.aliyuncs.com/"+img_name
    print(net_img_path+ '------%s' % threading.current_thread())
    local_img_path = 'C:\\juromeocommentimg_thread\\'
    os.makedirs(local_img_path, exist_ok=True)
    r = requests.get(net_img_path)
    with open(local_img_path+img_name, 'wb') as f:
        f.write(r.content)


def get_product_json_info(product_url):
    db = MySQLdb.connect("192.168.21.238", "root", "Wang123app", "juromeo_comment", charset='utf8')
    cursor = db.cursor()
    response = requests.get(product_url)
    response.encoding = response.apparent_encoding
    html_str = response.text
    html_str = html_str.split('var productJson =')[1]
    json_str = html_str.split('function displayProductImgs')[0]
    json_str = json_str.strip()
    json_str = json_str.replace("'","\\\'")
    json_str = json_str.replace('"','\\\"')
    #print(json_str)
    # SQL 插入语句
    sql = "INSERT INTO product_info(`url`, `product_info_json`) VALUES (%s, %s)" % ("'" + url + "'", "'" + json_str + "'")
    try:
        # 执行sql语句
        print(sql)
        result = cursor.execute(sql)
        print('db result:', result)
        # 提交到数据库执行
        db.commit()
    except:
        # 发生错误时回滚
        db.rollback()
    db.close()


db = MySQLdb.connect("192.168.21.238", "root", "Wang123app", "juromeo_comment", charset='utf8')
cursor = db.cursor()
sql = "select product_url from juromeo_product where product_url not in (select url from product_info)"
try:
   # 执行SQL语句
   cursor.execute(sql)
   # 获取所有记录列表
   results = cursor.fetchall()
   #print(len(results))
   for row in results:
        url = row[0]
        url = url.strip()
        get_product_json_info(url)
except:
   print("Error: unable to fecth data")

# 关闭数据库连接
db.close()

