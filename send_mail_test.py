#coding=utf-8
import time
from locale import *
import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup

file_name = "found_asin_"+datetime.datetime.now().strftime('%Y-%m-%d_%H_%M_%S')+".xls"

loginUrl = "https://login.live.com/"

username = input("Input userName:")
print("userName:", username)
password = input("Input passWord:")
print("passWord:", password)

setlocale(LC_NUMERIC, 'English_US')

'''
firefoxProfie = FirefoxProfile()
firefoxProfie.set_preference('permissions.default.image', 2)
driver = webdriver.Firefox(firefoxProfie)
'''
driver = webdriver.Firefox()
driver.maximize_window()
driver.get(loginUrl)

#用户名
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.NAME, 'loginfmt')))
driver.find_element_by_name("loginfmt").clear()
driver.find_element_by_name("loginfmt").send_keys(username)
driver.find_element_by_xpath('//*[@id="idSIButton9"]').click()
time.sleep(1)
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.NAME, 'loginfmt')))
#密码
driver.find_element_by_name("passwd").clear()
driver.find_element_by_name("passwd").send_keys(password)
WebDriverWait(driver, 60).until(EC.element_to_be_clickable((By.ID, 'idSIButton9')))
time.sleep(1)
driver.find_element_by_class_name('btn-primary').click()

#到邮箱界面
driver.get("https://outlook.live.com/mail/inbox")
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div/button')))
driver.find_element_by_xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div/button").click()

#收件人
receive_xpath = '/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[1]/div[1]/div/div[1]/div/div/span/input'
WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, receive_xpath)))
driver.find_element_by_xpath(receive_xpath).clear()
driver.find_element_by_xpath(receive_xpath).send_keys("474067570@qq.com")
#主题
driver.find_element_by_xpath('//*[@id="subjectLine0"]').click()
driver.find_element_by_xpath('//*[@id="subjectLine0"]').send_keys("test subjects")
time.sleep(2)
#内容
contentEle = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[2]/div[1]/div/div/div')
driver.execute_script("arguments[0].innerHTML = arguments[1]", contentEle, "test contents ssssssssss")
time.sleep(2)

clickEle = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[2]/div[1]/div/div')
ActionChains(driver).click(clickEle).perform()
time.sleep(2)
driver.find_element_by_xpath('//*[@id="subjectLine0"]').click()
driver.find_element_by_xpath('//*[@id="subjectLine0"]').send_keys("test subjects 2")
driver.find_element_by_xpath('//*[@id="subjectLine0"]').send_keys(Keys.TAB)
time.sleep(2)
ActionChains(driver).key_down(Keys.CONTROL, contentEle).send_keys('a').key_up(Keys.CONTROL).perform()
time.sleep(2)
ActionChains(driver).key_down(Keys.CONTROL, contentEle).send_keys('x').key_up(Keys.CONTROL).perform()
clickButton = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[2]/div[3]/div[1]/div/div[9]/button')
ActionChains(driver).click(clickButton).perform()
time.sleep(2)
ActionChains(driver).send_keys("test send kyes").perform()
time.sleep(2)
ActionChains(driver).release(contentEle).perform()
#ActionChains(driver).
#ActionChains(driver).send_keys_to_element(contentEle, 'aaaaaaaaaaaaaaaaa')
#ActionChains(driver).key_down('a', contentEle)
#ActionChains(driver).send_keys_to_element(contentEle,'ok')
#ActionChains(driver).click_and_hold(contentEle).perform()
#ActionChains(driver).double_click(contentEle).perform()
#contentEle.send_keys(Keys.SPACE)
time.sleep(2)
driver.switch_to.frame(0)
time.sleep(2)
driver.switch_to.parent_frame()
driver.switch_to.default_content()

time.sleep(2)
#发送
#driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[2]/div[3]/div[2]/div[1]/button[1]').click()
send_btn = driver.find_element_by_xpath('/html/body/div[2]/div/div[2]/div[1]/div[1]/div[3]/div[2]/div/div[3]/div[1]/div/div/div/div/div[2]/div[3]/div[2]/div[1]/button[1]')
ActionChains(driver).click(send_btn).perform()
time.sleep(5)
