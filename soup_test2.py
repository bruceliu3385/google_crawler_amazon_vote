import re
from locale import *
from bs4 import BeautifulSoup

setlocale(LC_NUMERIC, 'English_US')

html = """
<table cellspacing="0" cellpadding="0" border="0">
     <tbody>
         <tr><td class="label">ASIN</td><td class="value">B0769J3JWR</td></tr>







<tr class="shipping-weight"><td class="label">Produktgewicht inkl. Verpackung</td><td class="value">522 g</td></tr>













<tr class="item-model-number"><td class="label">Herstellerreferenz
</td><td class="value">Ataya</td></tr>













<tr class="date-first-available"><td class="label">Im Angebot von Amazon.de seit</td><td class="value">19. Oktober 2017</td></tr>





<tr class="average_customer_reviews">  <td class="label">Durchschnittliche Kundenbewertung</td>  <td class="value"><span class="dpProductDetailB0769J3JWR">
  <span class="a-declarative" data-action="a-popover" data-a-popover="{&quot;closeButton&quot;:&quot;false&quot;,&quot;max-width&quot;:&quot;700&quot;,&quot;position&quot;:&quot;triggerBottom&quot;,&quot;url&quot;:&quot;/review/widgets/average-customer-review/popover/ref=acr_dpproductdetail_popover?ie=UTF8&amp;asin=B0769J3JWR&amp;contextId=dpProductDetail&amp;ref=acr_dpproductdetail_popover&quot;}">
    <a href="javascript:void(0)" class="a-popover-trigger a-declarative">
      </a><a class="a-link-normal a-text-normal" href="https://www.amazon.de/product-reviews/B0769J3JWR/ref=acr_dpproductdetail_text?ie=UTF8&amp;showViewpoints=1">
        <i class="a-icon a-icon-star a-star-5"><span class="a-icon-alt">4.8 von 5 Sternen</span></i>
      </a>
    <i class="a-icon a-icon-popover"></i>
  </span>
  <span class="a-letter-space"></span>
  <span class="a-size-small">
    <a class="a-link-normal" href="https://www.amazon.de/product-reviews/B0769J3JWR/ref=acr_dpproductdetail_text?ie=UTF8&amp;showViewpoints=1">
      12 Kundenrezensionen
    </a>
  </span>
</span></td></tr>




























<tr id="SalesRank"><td class="label">Amazon Bestseller-Rang</td><td class="value">


















Nr. 635 in Küche, Haushalt &amp; Wohnen (<a href="https://www.amazon.de/gp/bestsellers/kitchen/ref=pd_dp_ts_kitchen_1">Siehe Top 100</a>)
 






















<ul class="zg_hrsr">
    <li class="zg_hrsr_item">
    <span class="zg_hrsr_rank">Nr. 15</span>
    <span class="zg_hrsr_ladder">in&nbsp;<a href="https://www.amazon.de/gp/bestsellers/kitchen/ref=pd_zg_hrsr_kitchen_1_1">Küche, Haushalt &amp; Wohnen</a> &gt; <a href="https://www.amazon.de/gp/bestsellers/kitchen/10176091/ref=pd_zg_hrsr_kitchen_1_2">Heimtextilien, Bad- &amp; Bettwaren</a> &gt; <a href="https://www.amazon.de/gp/bestsellers/kitchen/3340952031/ref=pd_zg_hrsr_kitchen_1_3">Bettwaren &amp; Bettwäsche</a> &gt; <a href="https://www.amazon.de/gp/bestsellers/kitchen/10399161/ref=pd_zg_hrsr_kitchen_1_4_last">Wohn- &amp; Kuscheldecken</a></span>
    </li>
</ul>
</td></tr>
<tr><td class="lAttr">&nbsp;</td><td class="lAttr">&nbsp;</td></tr>
     </tbody>
     </table>
"""
html = '''
<div class="content">







<ul>













                                                                                










<li><b>Produktgewicht inkl. Verpackung:</b> 998 g</li>









  


<li><b>ASIN:</b> B07BZ4S3VW</li>









      











    <li><b> Im Angebot von Amazon.de seit:</b> 5. August 2012</li>




































  
  




























<li><b>Durchschnittliche Kundenbewertung:</b> 




<span class="dpProductDetailB07BZ4S3VW">
  <span class="a-declarative" data-action="a-popover" data-a-popover="{&quot;closeButton&quot;:&quot;false&quot;,&quot;max-width&quot;:&quot;700&quot;,&quot;position&quot;:&quot;triggerBottom&quot;,&quot;url&quot;:&quot;/review/widgets/average-customer-review/popover/ref=acr_dpproductdetail_popover?ie=UTF8&amp;asin=B07BZ4S3VW&amp;contextId=dpProductDetail&amp;ref=acr_dpproductdetail_popover&quot;}">
    <a href="javascript:void(0)" class="a-popover-trigger a-declarative">
      </a><a class="a-link-normal a-text-normal" href="https://www.amazon.de/product-reviews/B07BZ4S3VW/ref=acr_dpproductdetail_text?ie=UTF8&amp;showViewpoints=1">
        <i class="a-icon a-icon-star a-star-5"><span class="a-icon-alt">5.0 von 5 Sternen</span></i>
      </a>
    <i class="a-icon a-icon-popover"></i>
  </span>
  <span class="a-letter-space"></span>
  <span class="a-size-small">
    <a class="a-link-normal" href="https://www.amazon.de/product-reviews/B07BZ4S3VW/ref=acr_dpproductdetail_text?ie=UTF8&amp;showViewpoints=1">
      1 Kundenrezension
    </a>
  </span>
</span>










</li>




























<li id="SalesRank">
<b>Amazon Bestseller-Rang:</b> 


















Nr. 136.483 in Schuhe &amp; Handtaschen (<a href="https://www.amazon.de/gp/bestsellers/shoes/ref=pd_dp_ts_shoes_1">Siehe Top 100 in Schuhe &amp; Handtaschen</a>)
 





















<style type="text/css">
.zg_hrsr { margin: 0; padding: 0; list-style-type: none; }
.zg_hrsr_item { margin: 0 0 0 10px; }
.zg_hrsr_rank { display: inline-block; width: 80px; text-align: right; }
</style>

<ul class="zg_hrsr">
    <li class="zg_hrsr_item">
    <span class="zg_hrsr_rank">Nr. 11785</span>
    <span class="zg_hrsr_ladder">in&nbsp;<a href="https://www.amazon.de/gp/bestsellers/shoes/ref=pd_zg_hrsr_shoes_1_1">Schuhe &amp; Handtaschen</a> &gt; <a href="https://www.amazon.de/gp/bestsellers/shoes/1760296031/ref=pd_zg_hrsr_shoes_1_2">Schuhe</a> &gt; <a href="https://www.amazon.de/gp/bestsellers/shoes/1760367031/ref=pd_zg_hrsr_shoes_1_3">Herren</a> &gt; <b><a href="https://www.amazon.de/gp/bestsellers/shoes/1760376031/ref=pd_zg_hrsr_shoes_1_4_last">Sneaker</a></b></span>
    </li>
</ul>

</li>





		
	
		
	
		
	
			
			
			
     	    
            
            
            
            
            
            <p></p><div class="bucket">    <script>
    P.now("A","tellMeMoreLinkData").execute(function(A,tellMeMoreLinkData){
        if(typeof tellMeMoreLinkData !== 'undefined'){
            A.state('lowerPricePopoverData',{"trigger":"ns_N3NDDYBV2N1W3JX21J43_1103_1_hmd_pricing_feedback_trigger_product-detail","destination":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoesencodeURI('&originalURI=' + window.location.pathname)","url":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoes","nsPrefix":"ns_N3NDDYBV2N1W3JX21J43_1103_2_","path":"encodeURI('&originalURI=' + window.location.pathname)","title":"Informieren Sie uns über einen günstigeren Preis"});
            return {
                   key:"pricing-fbW",
                   method: function(){
                                     return {"trigger":"ns_N3NDDYBV2N1W3JX21J43_1103_1_hmd_pricing_feedback_trigger_product-detail","destination":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoesencodeURI('&originalURI=' + window.location.pathname)","url":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoes","nsPrefix":"ns_N3NDDYBV2N1W3JX21J43_1103_2_","path":"encodeURI('&originalURI=' + window.location.pathname)","title":"Informieren Sie uns über einen günstigeren Preis"};
                                     }
                   }
        }
        else{
                P.when("A").register("tellMeMoreLinkData",function(A){
                    A.state('lowerPricePopoverData',{"trigger":"ns_N3NDDYBV2N1W3JX21J43_1103_1_hmd_pricing_feedback_trigger_product-detail","destination":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoesencodeURI('&originalURI=' + window.location.pathname)","url":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoes","nsPrefix":"ns_N3NDDYBV2N1W3JX21J43_1103_2_","path":"encodeURI('&originalURI=' + window.location.pathname)","title":"Informieren Sie uns über einen günstigeren Preis"});
                    return {
                           key:"pricing-fbW",
                           method: function(){
                                             return {"trigger":"ns_N3NDDYBV2N1W3JX21J43_1103_1_hmd_pricing_feedback_trigger_product-detail","destination":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoesencodeURI('&originalURI=' + window.location.pathname)","url":"/gp/pdp/pf/pricingFeedbackForm.html/ref=sr_1_1_sspa_pfdpb?ie=UTF8&ASIN=B07BZ4S3VW&PREFIX=ns_N3NDDYBV2N1W3JX21J43_1103_2_&WDG=shoes_display_on_website&dpRequestId=N3NDDYBV2N1W3JX21J43&from=product-detail&keywords=shoes&psc=1&qid=1544521529&sr=8-1-spons&storeID=shoes","nsPrefix":"ns_N3NDDYBV2N1W3JX21J43_1103_2_","path":"encodeURI('&originalURI=' + window.location.pathname)","title":"Informieren Sie uns über einen günstigeren Preis"};
                                             }
                           }
			    });
            }
    });
    </script>
Möchten Sie uns <b><a href="#" id="ns_N3NDDYBV2N1W3JX21J43_1103_1_hmd_pricing_feedback_trigger_product-detail" class="a-declarative">über einen günstigeren Preis informieren</a></b>?</div><p></p>











</ul>

<span class="tiny">
<ul class="noteBullets">









</ul>
</span>

  </div>
'''
soup = BeautifulSoup(html, "html.parser")
sales_rank_ele = soup.select_one('#SalesRank')
sales_rank = sales_rank_ele.get_text().strip()

#print('SALES_RANK:', sales_rank)
rank_tuple = re.findall(r'\d+[\.\d+]+', sales_rank)
if rank_tuple.__len__() == 0:
    print('RANK TUPLE EMPTY!!!')
rank_num = atof(rank_tuple[0].replace('.', ','))
rank_text = rank_tuple[0]
print("RANK NUM:", rank_num)
parent_ele = sales_rank_ele.parent
style_li = parent_ele.select('li')
style_tr = parent_ele.select('tr')
date_txt = ''
if len(style_li) > 2:
    date_ele = parent_ele.find(lambda e: 'Im Angebot von Amazon' in e.text)
    if date_ele is not None and (len(date_ele.contents) == 2):
        date_txt = date_ele.contents[1]
elif len(style_tr) > 2:
    date_ele_tr = parent_ele.select_one('tr.date-first-available')
    date_ele = date_ele_tr.select_one('td.value')
    date_txt = date_ele.get_text()
print('DATA TEXT:', date_txt)

#date_text = date_ele.p.contents[0].strip()
#print('DATE TEXT:', date_text)



